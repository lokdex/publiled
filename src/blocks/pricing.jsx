import * as React from "react";
import { Link } from "gatsby";

import { MdCheck, MdClear } from "react-icons/md";

export default function Block({ heading, text, items, ...props }) {
   return (
      <section py="4|6|12|20" {...props}>
         <div variant="container">
            <div textAlign="center">
               {heading && (
                  <h2 variant="heading.h1" lineHeight="1">
                     {heading}
                  </h2>
               )}
               {text && (
                  <p variant="text.lead" mt="4">
                     {text}
                  </p>
               )}
            </div>
            {items && (
               <div
                  display="grid"
                  col="1|2|3"
                  mt="4|6|8"
                  gap="4|6|0"
                  borderTopWidth="5"
                  borderTopColor="primary"
               >
                  {items.map((item, index) => (
                     <Pricing key={index} {...item} />
                  ))}
               </div>
            )}
         </div>
      </section>
   );
}

export function Pricing({
   subheading,
   heading,
   price,
   description,
   pros,
   cons,
   isSelected,
   ...props
}) {
   return (
      <div
         bg="background"
         textAlign="center"
         borderWidth="1px"
         px="4|4|8|10"
         py="8|8|12"
         {...props}
      >
         {subheading && <p variant="subheading">{subheading}</p>}
         <h3 variant="heading.h2" m="0" color="primary">
            {heading}
         </h3>
         <p variant="text.paragraph" mt="4" lineHeight="normal">
            {description}
         </p>
         <div display="flex" mt="6|8" alignItems="center" justifyContent="center">
            {price === "Contáctanos" ? (
               <>
                  <Link to="/contacto">
                     <div variant="button.primary.lg" mt="6">
                        Contáctanos
                     </div>
                  </Link>
               </>
            ) : (
               <>
                  <span fontSize="6xl" fontWeight="semibold">
                     S/ {price}
                  </span>
                  <span fontSize="xs" ml="4">
                     por mes
                  </span>
               </>
            )}
         </div>
         <ul listStyle="none" p="0" mt="4" flex="1">
            {pros.map((listItem, index) => (
               <li key={index} fontWeight="semibold" d="inline-flex" alignItems="center" mb="2">
                  <MdCheck
                     style={{
                        height: "1.25rem",
                        width: "1.25rem",
                        marginLeft: "auto",
                        marginRight: "0.5rem",
                        color: "#BF351A",
                     }}
                  />
                  {listItem}
               </li>
            ))}
            {cons.map((listItem, index) => (
               <li key={index} fontWeight="semibold" d="inline-flex" alignItems="center" mb="2">
                  <MdClear
                     style={{
                        height: "1.25rem",
                        width: "1.25rem",
                        marginLeft: "auto",
                        marginRight: "0.5rem",
                        color: "#BF351A",
                     }}
                  />
                  {listItem}
               </li>
            ))}
         </ul>
      </div>
   );
}
