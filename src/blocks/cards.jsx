import * as React from "react";
import { Icon } from "reflexjs";
import { Link } from "gatsby";

export default function Block({ buttons, cards, columns = 3, ...props }) {
   return (
      <section py="4|6|12|20" {...props}>
         <div variant="container">
            {cards && (
               <div display="grid" col={`1|2|${columns}`} gap="4|8" my="8|12">
                  {cards.map((card, index) => (
                     <Card key={index} {...card} />
                  ))}
               </div>
            )}
            {buttons}
         </div>
      </section>
   );
}

export function Card({ heading, text, icon, link, ...props }) {
   return (
      <div
         display="flex"
         flexDirection="column"
         textAlign="center"
         borderWidth="1"
         rounded="lg"
         p="8"
         {...props}
      >
         <div
            display="flex"
            alignItems="center"
            justifyContent="center"
            size="18"
            rounded="full"
            mb="4"
            mx="auto"
            bg="primary"
         >
            {icon}
         </div>
         <div flex="1">
            <h4 variant="heading.h3">{heading}</h4>
            {text && (
               <p variant="text.paragraph text.md" mt="1">
                  {text}
               </p>
            )}
            {link}
         </div>
      </div>
   );
}
