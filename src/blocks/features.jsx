import * as React from "react";
import { MdCheck } from "react-icons/md";

export default function Block({ features, columns = 3, ...props }) {
   return (
      <section py="4|6" {...props}>
         <div variant="container">
            {features && (
               <div display="grid" col={`1|2|${columns}`} gap="4|8" my="8|12">
                  {features.map((feature, index) => (
                     <Feature key={index} {...feature} />
                  ))}
               </div>
            )}
         </div>
      </section>
   );
}

export function Feature({ heading, text, icon, link, ...props }) {
   return (
      <div display="flex" flexDirection="column" textAlign="center" rounded="lg" p="6" {...props}>
         <div
            display="flex"
            alignItems="center"
            justifyContent="center"
            size="18"
            rounded="full"
            mb="4"
            mx="auto"
            bg="primary"
         >
            <MdCheck
               style={{
                  height: "2rem",
                  width: "2rem",
                  marginLeft: "auto",
                  marginRight: "auto",
                  color: "#fff",
               }}
            />
         </div>
         <div flex="1">
            <h4 variant="heading.h3">{heading}</h4>
            <p variant="text.paragraph text.md" mt="1">
               {text}
            </p>
         </div>
      </div>
   );
}
