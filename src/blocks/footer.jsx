import * as React from "react";
import { VisuallyHidden } from "reflexjs";

export default function Block({ name, copyright, links, iconLinks, children, ...props }) {
   return (
      <section py={[8, 10, 12]} {...props}>
         <div variant="container">
            <div
               display="flex"
               alignItems="center"
               flexDirection="column|row"
               justifyContent="space-between"
            >
               <div maxW="none|300">
                  {name && <h2 variant="heading.h2">{name}</h2>} {children}
               </div>
            </div>
            {copyright && (
               <div
                  display="flex"
                  alignItems="center"
                  justifyContent="space-between"
                  borderTopWidth={1}
                  textAlign="center"
                  pt="4|5|6"
                  mt="4|5|6"
               >
                  <p variant="text.sm" my="0">
                     {copyright}
                  </p>
                  {iconLinks?.length && (
                     <div display="grid" col="5" gap="4" mt="4|0">
                        {iconLinks.map((iconLink, index) => (
                           <a key={index} href={iconLink.href} color="text">
                              {iconLink.icon}
                              <VisuallyHidden>{iconLink.title}</VisuallyHidden>
                           </a>
                        ))}
                     </div>
                  )}
               </div>
            )}
         </div>
      </section>
   );
}
