import * as React from "react";

import { VisuallyHidden } from "reflexjs";

import Nav from "../components/navBar";
import Footer from "../components/footer";

import { BsPhone } from "react-icons/bs";
import { FiFacebook, FiTwitter, FiInstagram, FiLinkedin } from "react-icons/fi";
import { FaWhatsapp } from "react-icons/fa";

export default function ContactoPage() {
   return (
      <>
         <Nav />
         <section py="6|12|20">
            <div variant="container">
               <div
                  display="grid"
                  gridAutoFlow="dense"
                  col="1|1|2"
                  gap="8|8|12"
                  alignItems="center"
               >
                  <div
                     d="flex"
                     flexDirection="column"
                     alignItems="center|flex-start"
                     textAlign="center|left"
                  >
                     <h1 variant="heading.h1" fontWeight="bolder" lineHeight="tight">
                        Comunícate con nosotros
                     </h1>

                     <p variant="text.lead" mt="2">
                        Haz crecer tu negocio
                     </p>
                  </div>
                  <div display="flex" alignItems="flex-start">
                     <div
                        display="flex"
                        alignItems="center"
                        justifyContent="center"
                        size="18"
                        rounded="full"
                        mb="4"
                        mx="auto"
                        bg="primary"
                     >
                        <BsPhone
                           color="white"
                           style={{
                              height: "2rem",
                              width: "2rem",
                              display: "block",
                              marginLeft: "auto",
                              marginRight: "auto",
                           }}
                        />
                     </div>
                     <div flex="1" ml="4">
                        <h4 variant="heading.h3">PubliLED</h4>
                        <p variant="text.paragraph text.md" mt="1">
                           +51 919 474 350 <br />
                           contacto@publyled.com
                        </p>
                     </div>
                  </div>
               </div>
               <div
                  display="flex"
                  flexDirection="column"
                  textAlign="center"
                  justifyContent="center"
               >
                  <p variant="text.lead" mt="10" mb="0">
                     O mándanos un correo y nos pondremos en contacto contigo
                  </p>

                  <form
                     display="grid"
                     col="1|repeat(2, auto)"
                     gap="4"
                     mt="4"
                     w="80%"
                     ml="10%"
                     action="sendmail.php"
                     method="post"
                  >
                     <div colStart="span 2">
                        <VisuallyHidden>
                           <label htmlFor="name">Nombre</label>
                        </VisuallyHidden>
                        <input
                           variant="input"
                           type="text"
                           id="name"
                           name="name"
                           placeholder="Nombre"
                        />
                     </div>
                     <div>
                        <VisuallyHidden>
                           <label htmlFor="phone">Teléfono</label>
                        </VisuallyHidden>
                        <input
                           variant="input"
                           type="text"
                           id="phone"
                           name="phone"
                           placeholder="Teléfono"
                        />
                     </div>
                     <div>
                        <VisuallyHidden>
                           <label htmlFor="email">Email</label>
                        </VisuallyHidden>
                        <input
                           variant="input"
                           type="email"
                           id="email"
                           name="email"
                           placeholder="Email"
                        />
                     </div>

                     <div colStart="span 2">
                        <VisuallyHidden>
                           <label htmlFor="message">Mensaje</label>
                        </VisuallyHidden>
                        <textarea
                           variant="textarea"
                           placeholder="Mensaje"
                           id="message"
                           name="message"
                           rows="5"
                        />
                     </div>
                     <button type="submit" variant="button.primary" colStart="span 2">
                        Enviar
                     </button>
                  </form>
                  <h3 variant="heading.h2" mt="10">
                     Visita nuestras redes sociales
                  </h3>
                  <div display="grid" col={`1|2`} gap="0" my="8|12" w="60%" ml="20%">
                     <a
                        href="https://www.facebook.com/Publi-Led-109006321012554"
                        target="_blank"
                        display="flex"
                        alignItems="center"
                        justifyContent="center"
                        size="18"
                        rounded="full"
                        mb="4"
                        mx="auto"
                        bg="primary"
                     >
                        <FiFacebook
                           color="white"
                           style={{
                              height: "2rem",
                              width: "2rem",
                              display: "block",
                              marginLeft: "auto",
                              marginRight: "auto",
                           }}
                        />
                     </a>
                     <a
                        href="https://www.instagram.com/publi_ledaqp/"
                        target="_blank"
                        display="flex"
                        alignItems="center"
                        justifyContent="center"
                        size="18"
                        rounded="full"
                        mb="4"
                        mx="auto"
                        bg="primary"
                     >
                        <FiInstagram
                           color="white"
                           style={{
                              height: "2rem",
                              width: "2rem",
                              display: "block",
                              marginLeft: "auto",
                              marginRight: "auto",
                           }}
                        />
                     </a>
                     <a
                        href="https://twitter.com/led_publi"
                        target="_blank"
                        display="flex"
                        alignItems="center"
                        justifyContent="center"
                        size="18"
                        rounded="full"
                        mb="4"
                        mx="auto"
                        bg="primary"
                     >
                        <FiTwitter
                           color="white"
                           style={{
                              height: "2rem",
                              width: "2rem",
                              display: "block",
                              marginLeft: "auto",
                              marginRight: "auto",
                           }}
                        />
                     </a>
                     <a
                        href="https://www.linkedin.com/in/publiled-aqp-b0baa71bb/"
                        target="_blank"
                        display="flex"
                        alignItems="center"
                        justifyContent="center"
                        size="18"
                        rounded="full"
                        mb="4"
                        mx="auto"
                        bg="primary"
                     >
                        <FiLinkedin
                           color="white"
                           style={{
                              height: "2rem",
                              width: "2rem",
                              display: "block",
                              marginLeft: "auto",
                              marginRight: "auto",
                           }}
                        />
                     </a>
                  </div>
                  <h3 variant="heading.h2" mt="10">
                     Escríbenos por WhatsApp
                  </h3>
                  <a
                     href="https://wa.me/message/D2AK2FWHO533M1"
                     target="_blank"
                     display="flex"
                     alignItems="center"
                     justifyContent="center"
                     size="18"
                     rounded="full"
                     mb="4"
                     mt="4"
                     mx="auto"
                     bg="primary"
                  >
                     <FaWhatsapp
                        color="white"
                        style={{
                           height: "2rem",
                           width: "2rem",
                           display: "block",
                           marginLeft: "auto",
                           marginRight: "auto",
                        }}
                     />
                  </a>
               </div>
            </div>
         </section>
         <Footer />
      </>
   );
}
