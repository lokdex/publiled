import * as React from "react";
import { Link } from "gatsby";

import Nav from "../components/navBar";
import Footer from "../components/footer";
import Planes from "../blocks/pricing";
import Call from "../blocks/call";

import { BsEnvelope } from "react-icons/bs";

export default function PlanesPage() {
   return (
      <>
         <Nav />
         <Planes
            heading="Nuestros Planes"
            text="Contamos con opciones que se adaptan a tu negocio"
            items={[
               {
                  heading: "Marketing Digital",
                  subheading: "Redes Sociales",
                  description: "Incrementa tus ventas",
                  pros: [
                     "Manejamos tu marketing en redes sociales",
                     "3 anuncios por semana en Instagram y Facebook",
                     "Incluye elaboración de anuncio",
                  ],
                  cons: ["NO incluye gastos en la red social"],
                  price: "500",
               },
               {
                  subheading: "Pantalla LED Móvil",
                  heading: "Exclusivo",
                  description: "La innovación en tu negocio",
                  pros: ["Dimensiones 1 x 3", "Instalación única vez de 500 soles"],
                  cons: ["Consumo de energía corre por cuenta del cliente"],
                  price: "3000",
               },
               {
                  subheading: "Pantalla LED",
                  heading: "Económico",
                  description: "Refuerza tu marca",
                  pros: [
                     "20 spots por hora",
                     "8 segundos x spot",
                     "240 spots por dia",
                     "1680 spots por semana",
                     "7200 spots por mes",
                  ],
                  cons: [],
                  price: "2500",
               },
               {
                  subheading: "Pantalla LED",
                  heading: "Intenso",
                  description: "Llega a todos",
                  pros: [
                     "50 spots por hora",
                     "8 segundos x spot",
                     "600 spots por dia",
                     "4200 spots por semana",
                     "18000 spots por semana",
                  ],
                  cons: [],
                  price: "6000",
               },
               {
                  subheading: "Letrero Fijo",
                  heading: "Ambas Caras",
                  description: "Incrementa tus ventas",
                  pros: ["Contamos con las mejores ubicaciones"],
                  cons: ["NO incluye banner o arte"],
                  price: "3000",
               },
               {
                  subheading: "Letrero LED",
                  heading: "Venta",
                  description: "Innova tu negocio",
                  pros: [
                     "Tenemos los mejores precios para tu propia pantalla LED",
                     "Te la instalamos y enseñamos a operar",
                     "Adaptado a tus necesidades",
                  ],
                  cons: [],
                  price: "Contáctanos",
               },
            ]}
         />
         <Call
            heading="Necesitas flexibilidad?"
            text="Contáctanos para personalizar tu propuesta"
            buttons={
               <Link to="/contacto">
                  <div variant="button.primary.lg" mt="6">
                     <BsEnvelope />
                     <span ml="2">Contáctanos</span>
                  </div>
               </Link>
            }
         />
         <Footer />
      </>
   );
}
