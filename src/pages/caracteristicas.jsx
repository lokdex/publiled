import * as React from "react";

import Nav from "../components/navBar";
import Footer from "../components/footer";
import Features from "../blocks/features";

import Pantalla from "../images/pantalla_size.png";
import Paneles from "../images/paneles.jpeg";

export default function CaracteristicasPage() {
   return (
      <>
         <Nav />
         <section pt="6|8|12|20">
            <div variant="container">
               <div
                  display="flex"
                  flexDirection="column"
                  textAlign="center"
                  justifyContent="center"
               >
                  <h2 variant="heading.h1" fontWeight="bold">
                     Nuestras Pantallas
                  </h2>
                  <p variant="text.lead" mt="2" mb="10">
                     Para que todos vean tu marca
                  </p>
                  <img src={Pantalla} w="80%" ml="10%" rounded="lg" overflow="hidden" />
                  <Features
                     text="Discover the tool that drives engagement and productivity."
                     features={[
                        {
                           heading: "Pantallas y paneles",
                           text: "Ubicados en puntos estratégicos de la ciudad",
                        },
                        {
                           heading: "Máxima Calidad",
                           text: "Pantallas modernas con la mejor calidad",
                        },
                        {
                           heading: "Alta Resolución",
                           text: "250 000 pixeles de resolución",
                        },
                        {
                           heading: "Brillo",
                           text: "Más de 150 000 candelas",
                        },
                        {
                           heading: "Eco-amigables",
                           text: "Iluminación LED para el menor consumo de energía",
                        },
                        {
                           heading: "Amplias",
                           text: "Con un área de 25.80 m2 tus clientes no se perderán ningún detalle",
                        },
                     ]}
                  />
                  <h2 variant="heading.h1" fontWeight="bold">
                     Nuestros Paneles Fijos
                  </h2>
                  <p variant="text.lead" mt="2" mb="10">
                     Para que tu marca se vea constantemente
                  </p>
                  <img src={Paneles} w="80%" ml="10%" rounded="lg" overflow="hidden" />
                  <p variant="text.lead" mt="2" mb="10">
                     De 4 a 6 metros de alto x 10 a 12 metros de largo
                  </p>
               </div>
            </div>
         </section>
         <Footer />
      </>
   );
}
