import * as React from "react";

import { VisuallyHidden } from "reflexjs";

import Nav from "../components/navBar";
import Footer from "../components/footer";

import { BsAward, BsPhone } from "react-icons/bs";
import { FiFacebook, FiTwitter, FiInstagram, FiLinkedin } from "react-icons/fi";
import { FaWhatsapp } from "react-icons/fa";

export default function NosotrosPage() {
   return (
      <>
         <Nav />
         <section py="6|12|20">
            <div variant="container">
               <div
                  display="flex"
                  flexDirection="column"
                  textAlign="center"
                  justifyContent="center"
               >
                  <h1 variant="heading.h1" fontWeight="bolder" lineHeight="tight">
                     Haz realidad tus sueños con PubliLED
                  </h1>
                  <p variant="text.lead" mt="2">
                     Déjanos ser tus socios para apoyarte en el desarrollo de tu negocio
                  </p>
               </div>

               <div display="flex" flexDirection="column" textAlign="center" rounded="lg" p="8">
                  <div
                     display="flex"
                     alignItems="center"
                     justifyContent="center"
                     size="18"
                     rounded="full"
                     mb="4"
                     mx="auto"
                     bg="primary"
                  >
                     <BsAward
                        color="white"
                        style={{
                           height: "2rem",
                           width: "2rem",
                           display: "block",
                           marginLeft: "auto",
                           marginRight: "auto",
                        }}
                     />
                  </div>

                  <div flex="1">
                     <h4 variant="heading.h4">Visión</h4>
                     <p variant="text.paragraph text.md" mt="1">
                        Constituirnos en un medio de comunicación exterior especializado en
                        marketing en Perú
                     </p>
                  </div>
               </div>
               <div display="flex" flexDirection="column" textAlign="center" rounded="lg" p="8">
                  <div
                     display="flex"
                     alignItems="center"
                     justifyContent="center"
                     size="18"
                     rounded="full"
                     mb="4"
                     mx="auto"
                     bg="primary"
                  >
                     <BsAward
                        color="white"
                        style={{
                           height: "2rem",
                           width: "2rem",
                           display: "block",
                           marginLeft: "auto",
                           marginRight: "auto",
                        }}
                     />
                  </div>

                  <div flex="1">
                     <h4 variant="heading.h4">Quiénes somos?</h4>
                     <p variant="text.paragraph text.md" mt="1">
                        Publiled Innovaciones S.A.C. – (PubliLED) con RUC No. 20606056665, es una
                        compañía dedicada a la actividad de exposición de spots publicitarios en
                        pantallas tipo LED de última generación, letreros fijos, impulsamos tu
                        negocio por medio de las redes sociales y te ofrecemos en venta los mejores
                        productos innovadores para publicidad, como tu propia pantalla LED para tu
                        negocio.
                        <br /> Disponemos de un equipo que esta atento para ayudarte y diseñar la
                        mejor opción para tu negocio.
                     </p>
                  </div>
               </div>
            </div>
         </section>
         <Footer />
      </>
   );
}
