import * as React from "react";

import Nav from "../components/navBar";
import Footer from "../components/footer";

export default function MapaPage() {
   return (
      <>
         <Nav />
         <section pt="6|8|12|20">
            <div variant="container">
               <div
                  display="flex"
                  flexDirection="column"
                  textAlign="center"
                  justifyContent="center"
               >
                  <h2 variant="heading.h1" fontWeight="bold">
                     Ubicación de nuestras pantallas
                  </h2>
                  <p variant="text.lead" mt="2" mb="10">
                     Participa en el mejor punto moderno de publicidad en Arequipa
                  </p>
               </div>
               <div display="grid" gridAutoFlow="dense" col="1|1|2" gap="0" alignItems="center">
                  <iframe
                     src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1999.5084546631765!2d-71.52157808066886!3d-16.411598048770784!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMTbCsDI0JzQyLjQiUyA3McKwMzEnMTYuMCJX!5e0!3m2!1sen!2sus!4v1632282865452!5m2!1sen!2sus"
                     width="500"
                     height="350"
                     style={{ border: 0 }}
                     allowfullscreen
                     loading="lazy"
                  ></iframe>
                  <div d="flex" flexDirection="column" alignItems="center" textAlign="center|left">
                     <h2
                        variant="heading.h2"
                        fontWeight="bolder"
                        lineHeight="tight"
                        color="primary"
                     >
                        Frente al Parque Lambramani
                     </h2>
                     <p variant="text.lead" mt="2">
                        En un intercambio vial altamente circulado todos los días
                     </p>
                  </div>
               </div>
               <div display="grid" gridAutoFlow="dense" col="1|1|2" gap="0" alignItems="center">
                  <div d="flex" flexDirection="column" alignItems="center" textAlign="center|left">
                     <h2
                        variant="heading.h2"
                        fontWeight="bolder"
                        lineHeight="tight"
                        color="primary"
                     >
                        Cruce Av. Ejército y Av. Cayma
                     </h2>
                     <p variant="text.lead" mt="2">
                        En una de las intersecciones más transitadas de la ciudad
                     </p>
                  </div>
                  <iframe
                     src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1913.8485560397937!2d-71.5479299!3d-16.3893847!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x91424a42143471f3%3A0x74d86a1dcb7d30b1!2sEj%C3%A9rcito%20796%2C%20Cayma%2004017!5e0!3m2!1ses!2spe!4v1632282634350!5m2!1ses!2spe"
                     width="500"
                     height="350"
                     style={{ border: 0 }}
                     allowfullscreen
                     loading="lazy"
                  ></iframe>
               </div>
               <div display="grid" gridAutoFlow="dense" col="1|1|2" gap="0" alignItems="center">
                  <iframe
                     src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d956.8770288123247!2d-71.534837!3d-16.3990008!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x91424a5731032971%3A0x487a310adbc441bb!2sC.%20Mercaderes%20230%2C%20Arequipa%2004001!5e0!3m2!1ses!2spe!4v1632351457770!5m2!1ses!2spe"
                     width="500"
                     height="350"
                     style={{ border: 0 }}
                     allowfullscreen
                     loading="lazy"
                  ></iframe>
                  <div d="flex" flexDirection="column" alignItems="center" textAlign="center|left">
                     <h2
                        variant="heading.h2"
                        fontWeight="bolder"
                        lineHeight="tight"
                        color="primary"
                     >
                        Galerías Gamesa
                     </h2>
                     <p variant="text.lead" mt="2">
                        En la entrada al pase peatonal central de la ciudad
                     </p>
                  </div>
               </div>
               <div
                  display="flex"
                  flexDirection="column"
                  textAlign="center"
                  justifyContent="center"
               >
                  <p variant="text.lead" mt="6" mb="10">
                     También contamos con una pantalla en la ciudad de Tacna
                  </p>
               </div>
               <div display="grid" gridAutoFlow="dense" col="1|1|2" gap="0" alignItems="center">
                  <div d="flex" flexDirection="column" alignItems="center" textAlign="center|left">
                     <h2
                        variant="heading.h2"
                        fontWeight="bolder"
                        lineHeight="tight"
                        color="primary"
                     >
                        Av. Coronel Mendoza & Av. Gustavo Pinto
                     </h2>
                     <p variant="text.lead" mt="2">
                        En el cruce de mayor movimiento comercial de la ciudad
                     </p>
                  </div>
                  <iframe
                     src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1393.676233841215!2d-70.24329405319338!3d-18.003742799039582!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x915acfd1e61c8e0b%3A0xcc5abcec17b0b40c!2sPubliLED%20LED%204!5e0!3m2!1ses!2spe!4v1633998456820!5m2!1ses!2spe"
                     width="500"
                     height="350"
                     style={{ border: 0 }}
                     allowfullscreen
                     loading="lazy"
                  ></iframe>
               </div>
               <div
                  display="flex"
                  flexDirection="column"
                  textAlign="center"
                  justifyContent="center"
               >
                  <p variant="text.lead" mt="6" mb="10">
                     Y también tenemos presencia en la ciudad de Cusco
                  </p>
               </div>
               <div display="grid" gridAutoFlow="dense" col="1|1|2" gap="0" alignItems="center">
                  <iframe
                     src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d847.1615753319132!2d-71.96907909604005!3d-13.531585692953167!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x916dd5e5bf77e5b5%3A0xbe3eef67477e5eaf!2s%C3%93valo%20de%20Pachacutec!5e0!3m2!1ses!2spe!4v1633998610123!5m2!1ses!2spe"
                     width="500"
                     height="350"
                     style={{ border: 0 }}
                     allowfullscreen
                     loading="lazy"
                  ></iframe>
                  <div d="flex" flexDirection="column" alignItems="center" textAlign="center|left">
                     <h2
                        variant="heading.h2"
                        fontWeight="bolder"
                        lineHeight="tight"
                        color="primary"
                     >
                        Óvalo Pachacutec
                     </h2>
                     <p variant="text.lead" mt="2">
                        El óvalo más transitado de la ciudad
                     </p>
                  </div>
               </div>
               <div
                  display="flex"
                  flexDirection="column"
                  textAlign="center"
                  justifyContent="center"
               >
                  <h2 variant="heading.h2" fontWeight="bold" mt="10">
                     Ubicación de nuestros paneles fijos
                  </h2>
                  <p variant="text.lead" mt="2" mb="10">
                     Contamos con paneles en estas ubicaciones:
                  </p>
               </div>
               <div>
                  <p variant="text.lead" mt="2" color="primary">
                     Arequipa
                  </p>
                  <ul style={{ listStyle: "square outside" }}>
                     <li>
                        <span style={{ fontWeight: "900" }}>&#8226;</span> Óvalo del Parque
                        Lambramani
                     </li>
                     <li>
                        <span style={{ fontWeight: "900" }}>&#8226;</span> Frente a City Center
                     </li>
                     <li>
                        <span style={{ fontWeight: "900" }}>&#8226;</span> Variante de Uchumayo a la
                        altura del puente
                     </li>
                     <li>
                        <span style={{ fontWeight: "900" }}>&#8226;</span> Av. Andrés Avelino
                        Cáceres al frente del Centro Comercial
                     </li>
                     <li>
                        <span style={{ fontWeight: "900" }}>&#8226;</span> Av. Venezuela área
                        Financiera
                     </li>
                     <li>
                        <span style={{ fontWeight: "900" }}>&#8226;</span> Av. Los Incas
                     </li>
                     <li>
                        <span style={{ fontWeight: "900" }}>&#8226;</span> Av. Salaverry con 28 de
                        Julio
                     </li>
                     <li>
                        <span style={{ fontWeight: "900" }}>&#8226;</span> Vía Evitamiento con Av.
                        Aviación
                     </li>
                     <li>
                        <span style={{ fontWeight: "900" }}>&#8226;</span> Vía Evitamiento frente a
                        Metro
                     </li>
                     <li>
                        <span style={{ fontWeight: "900" }}>&#8226;</span> Av. Parra frente al BCP
                     </li>
                     <li>
                        <span style={{ fontWeight: "900" }}>&#8226;</span> Av. Parra frente al
                        Palacio de Bellas Artes
                     </li>
                     <li>
                        <span style={{ fontWeight: "900" }}>&#8226;</span> Av. Parra
                     </li>
                     <li>
                        <span style={{ fontWeight: "900" }}>&#8226;</span> Av. Jesús con Av. Lima
                     </li>
                     <li>
                        <span style={{ fontWeight: "900" }}>&#8226;</span> Puente San Martín en la
                        subida hacia la U. Católica
                     </li>
                  </ul>
               </div>
               <div>
                  <p variant="text.lead" mt="2" color="primary">
                     Camaná
                  </p>
                  <ul style={{ listStyle: "square outside" }}>
                     <li>
                        <span style={{ fontWeight: "900" }}>&#8226;</span> Rotonda de ingreso
                     </li>
                  </ul>
               </div>
               <div>
                  <p variant="text.lead" mt="2" color="primary">
                     Ica
                  </p>
                  <ul style={{ listStyle: "square outside" }}>
                     <li>
                        <span style={{ fontWeight: "900" }}>&#8226;</span> Panamericana Sur,
                        Tanjalla
                     </li>
                  </ul>
               </div>
               <div>
                  <p variant="text.lead" mt="2" color="primary">
                     Tacna
                  </p>
                  <ul style={{ listStyle: "square outside" }}>
                     <li>
                        <span style={{ fontWeight: "900" }}>&#8226;</span> Salida a Moquegua
                        (Panamericana Sur)
                     </li>
                     <li>
                        <span style={{ fontWeight: "900" }}>&#8226;</span> Ingreso a Tacna, óvalo
                        Cristo Rey
                     </li>
                     <li>
                        <span style={{ fontWeight: "900" }}>&#8226;</span> Frontera con Chile
                     </li>
                     <li>
                        <span style={{ fontWeight: "900" }}>&#8226;</span> Av. Collpa, 2 letreros
                        (carretera Costanera Tacna - Ilo - Matarani)
                     </li>
                  </ul>
               </div>
            </div>
         </section>
         <Footer />
      </>
   );
}
