import * as React from "react";
import { Link } from "gatsby";

import Nav from "../components/navBar";
import Footer from "../components/footer";

import Hero from "../blocks/hero";
import Cards from "../blocks/cards";
import Call from "../blocks/call";

import LogoHero from "../images/logo_welcome.png";

import { BsArrowRight, BsGridFill, BsListUl, BsMap, BsEnvelope } from "react-icons/bs";

export default function IndexPage() {
   return (
      <>
         <Nav />
         <Hero
            heading="PubliLED"
            text="La publicidad cambió, anúnciate con nosotros en nuestras pantallas LED de última
            generación"
            image={{
               src: LogoHero,
               alt: "Panel",
            }}
            buttons={
               <div display="inline-grid" col="2" gap="2" mt="4">
                  <a href="http://publyled.com/brochure.pdf" target="_blank">
                     <button variant="button.primary">Consulta nuestro brochure</button>
                  </a>
                  <Link to="/planes">
                     <button
                        variant="button"
                        color="primary"
                        style={{ border: "1px", borderColor: "#BF351A" }}
                     >
                        Nuestros planes <BsArrowRight size="30" />
                     </button>
                  </Link>
               </div>
            }
         />
         <Cards
            cards={[
               {
                  heading: "Mapa",
                  text: "Conoce la ubicación de nuestras pantallas",
                  icon: (
                     <BsMap
                        color="white"
                        style={{
                           height: "2rem",
                           width: "2rem",
                           display: "block",
                           marginLeft: "auto",
                           marginRight: "auto",
                        }}
                     />
                  ),
                  link: (
                     <Link to="/mapa">
                        <div display="inline-flex" alignItems="center" color="primary">
                           Ver más <BsArrowRight />
                        </div>
                     </Link>
                  ),
               },
               {
                  heading: "Características",
                  text: "Conoce más acerca de nuestras pantallas",
                  icon: (
                     <BsListUl
                        color="white"
                        style={{
                           height: "2rem",
                           width: "2rem",
                           display: "block",
                           marginLeft: "auto",
                           marginRight: "auto",
                        }}
                     />
                  ),
                  link: (
                     <Link to="/caracteristicas">
                        <div display="inline-flex" alignItems="center" color="primary">
                           Ver más <BsArrowRight />
                        </div>
                     </Link>
                  ),
               },
               {
                  heading: "Planes",
                  text: "Conoce los planes de pauta y spots que ofrecemos",
                  icon: (
                     <BsGridFill
                        color="white"
                        style={{
                           height: "2rem",
                           width: "2rem",
                           display: "block",
                           marginLeft: "auto",
                           marginRight: "auto",
                        }}
                     />
                  ),
                  link: (
                     <Link to="/planes">
                        <div display="inline-flex" alignItems="center" color="primary">
                           Ver más <BsArrowRight />
                        </div>
                     </Link>
                  ),
               },
            ]}
         />
         <Call
            heading="Listo para hacer crecer tu marca?"
            text="Aproximadamente 3 millones de personas al mes ven nuestras pantallas"
            buttons={
               <Link to="/contacto">
                  <div variant="button.primary.lg" mt="6">
                     <BsEnvelope />
                     <span ml="2">Contáctanos</span>
                  </div>
               </Link>
            }
         />
         <Footer />
      </>
   );
}
