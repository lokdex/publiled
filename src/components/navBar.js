import * as React from "react";
import Block from "../blocks/header";

export default function Example() {
   return (
      <Block
         branding={{
            name: "PubliLED",
            icon: "ojo.png",
         }}
         links={[
            {
               title: "Inicio",
               href: "/",
            },
            {
               title: "Planes",
               href: "/planes",
            },
            {
               title: "Mapa",
               href: "/mapa",
            },
            {
               title: "Características",
               href: "/caracteristicas",
            },
            {
               title: "Nosotros",
               href: "/nosotros",
            },
            {
               title: "Contacto",
               href: "/contacto",
            },
         ]}
      />
   );
}
