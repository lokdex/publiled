import * as React from "react";
import Block from "../blocks/footer";
import { Link } from "gatsby";

import { BsEnvelope } from "react-icons/bs";
import { FiFacebook, FiTwitter, FiInstagram, FiLinkedin } from "react-icons/fi";
import { FaWhatsapp } from "react-icons/fa";

export default function Example() {
   return (
      <Block
         name="PubliLED"
         iconLinks={[
            {
               title: "Follow on Facebook",
               href: "https://www.facebook.com/Publi-Led-109006321012554",
               icon: (
                  <FiFacebook
                     style={{
                        height: "1.25rem",
                        width: "1.25rem",
                        display: "block",
                        marginLeft: "auto",
                        marginRight: "auto",
                     }}
                  />
               ),
            },
            {
               title: "Follow on Instagram",
               href: "https://www.instagram.com/publi_ledaqp/",
               icon: (
                  <FiInstagram
                     style={{
                        height: "1.25rem",
                        width: "1.25rem",
                        display: "block",
                        marginLeft: "auto",
                        marginRight: "auto",
                     }}
                  />
               ),
            },
            {
               title: "Follow on Twitter",
               href: "https://twitter.com/led_publi",
               icon: (
                  <FiTwitter
                     style={{
                        height: "1.25rem",
                        width: "1.25rem",
                        display: "block",
                        marginLeft: "auto",
                        marginRight: "auto",
                     }}
                  />
               ),
            },
            {
               title: "Follow on Linked In",
               href: "https://www.linkedin.com/in/publiled-aqp-b0baa71bb/",
               icon: (
                  <FiLinkedin
                     style={{
                        height: "1.25rem",
                        width: "1.25rem",
                        display: "block",
                        marginLeft: "auto",
                        marginRight: "auto",
                     }}
                  />
               ),
            },
            {
               title: "Follow on WhatsApp",
               href: "https://wa.me/message/D2AK2FWHO533M1",
               icon: (
                  <FaWhatsapp
                     style={{
                        height: "1.25rem",
                        width: "1.25rem",
                        display: "block",
                        marginLeft: "auto",
                        marginRight: "auto",
                     }}
                  />
               ),
            },
         ]}
         copyright={`© ${new Date().getFullYear()} PubliLED`}
      >
         <Link to="/contacto">
            <div
               variant="text"
               display="inline-flex"
               alignItems="center"
               _hover={{
                  color: "primary",
               }}
            >
               <BsEnvelope
                  style={{
                     height: "1.5rem",
                     width: "1.5rem",
                     display: "block",
                     marginLeft: "auto",
                     marginRight: "auto",
                  }}
               />{" "}
               <span ml="2">contacto@publyled.com</span>
            </div>
         </Link>
      </Block>
   );
}
