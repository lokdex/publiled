require(`dotenv`).config();

module.exports = {
   siteMetadata: {
      title: "PubliLED",
      description: "Sitio web de PubliLED.",
      siteUrl: process.env.SITE_URL || "http://localhost:8000",
   },
   plugins: [`gatsby-plugin-reflexjs`, `gatsby-plugin-fontawesome-css`],
};
